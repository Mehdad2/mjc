import React, { Component } from 'react';
import RouteMatchRole from 'mjc-common/src/components/User/RouteRole'
import { Router } from 'react-router-dom'
import history from 'mjc-common/src/config/history'

class App extends Component {
  render() {
    return (
      <div>
        <Router history={history}>
          <RouteMatchRole/>
        </Router>
      </div>
    );
  }
}
export default App;
