import React from 'react';
import { BooleanInput } from 'react-admin';

const specialtiesCustom = (api) => 
{ 
  const specialties = api.resources.find(({name}) => 'specialties' === name);
  specialitiesBoolean(specialties)
}

const specialitiesBoolean = (specialties) => {
  var boolGroup = specialties.fields.find(({f})  =>  'isGroup' === f.name);
  boolGroup.input = (props) => (
    <BooleanInput
    source="isGroup"
    defaultValue={false}
  />
  );
}
export default specialtiesCustom
