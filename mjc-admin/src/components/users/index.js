import React from 'react';
import { TextInput, ArrayInput, SimpleFormIterator, BooleanInput } from 'react-admin';
import UserIcon from '@material-ui/icons/People';



const usersCustom = (api) => {
  const users = api.resources.find(({ name }) => 'users' === name);
  rolesUser(users)
  userBoolean(users)
  users.icon = UserIcon

}

const rolesUser = (users) => {
  const roles = users.fields.find(f => 'roles' === f.name);
    roles.input = props => (
    <ArrayInput source="roles">
      <SimpleFormIterator>
      <TextInput/>
      </SimpleFormIterator>
    </ArrayInput>
    );

    roles.input.defaultProps = {
      addfield: 'true',
      addLabel: false
    };
}

const userBoolean = (users) => {
  // Boolean IsActive
  var boolActive = users.fields.find(f  =>  'isActive' === f.name);
  boolActive.input = props => (
    <BooleanInput
    source="isActive"
    defaultValue={false}
  />
  );
  boolActive.input.defaultProps = {
    addfield: 'true',
    addLabel: true
  };
}




export default usersCustom
