import React, { FunctionComponent } from 'react'
import {  useStoreState } from "mjc-common/src/store"
import AppApi from '../../app/AppApi'

const AppAdmin: FunctionComponent = () => {
     // we want to retireve the role of the user from the userInformation
    const userData:any =  useStoreState(state => state.UserData.userInformation)

    //si le role est récupérer du state problème au refresh de la page
    const isAdmin = userData.role && userData.role.indexOf('ROLE_ADMIN')!= -1

    return <>
    {isAdmin && <AppApi/>}
    </>

}

export default AppAdmin