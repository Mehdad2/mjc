import React from 'react';
import { BooleanInput, SelectInput } from 'react-admin';
//weekArray
const WeekDays = ['isMonday', 'isTuesday','isWednesday', 'isThursday', 'isFriday', 'isSaturday', 'isSunday'] 

const configurationsCustom = (api) => {
    const configurations = api.resources.find(({ name }) =>  'configurations' === name);
    configurationBoolean(configurations)
    zoneInput(configurations)
}


// Booleans default value = false

const configurationBoolean = (configurations) => {
    WeekDays.map((element, index) => {
        console.log(element)
        let boolConf = configurations.fields.find(f  =>  element === f.name)

        boolConf.input = props => (
            <BooleanInput
            source={element}
            defaultValue={false}
            />
            );
    
        boolConf.input.defaultProps = {
            addField: true,
            addLabel: false
        };
        
    })
 
}

const zoneInput = (configurations) => {
    const zone = configurations.fields.find(f  =>  'zone' === f.name);

    zone.input  =  props  => (
    <SelectInput  source="zone"  choices={[
        { id: "A", name: 'Zone A' },
        { id: "B", name: 'Zone B' },
        { id: "C", name: 'Zone C' },
    ]}  />
    );

    zone.input.defaultProps = {
    addField: true,
    addLabel: false
    };
}



export default configurationsCustom