import React, { FunctionComponent , useEffect} from "react"
import { useStoreActions, useStoreDispatch, useStoreState } from "mjc-common/src/store"


const ActionGetTeacher: FunctionComponent  = () =>  {
    useStoreDispatch().RegistrationData.getTeacher({})
    const getTeacher = useStoreActions( actions => actions.RegistrationData.getTeacher)
     const teacher = useStoreState(state => state.RegistrationData.teacherData)
    useEffect(() => {
            getTeacher({})
      })
    return <> {getTeacher({})}</>

}

export default ActionGetTeacher

   