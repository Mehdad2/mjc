import React, { useEffect, FunctionComponent } from 'react';
import { SelectInput, ReferenceInput, SelectArrayInput,SimpleFormIterator, TextInput, ReferenceArrayInput,ReferenceArrayField,SingleFieldList, FunctionField, FormDataConsumer } from 'react-admin';
import {  DateTimeInput } from 'react-admin-date-inputs';
import { store } from "mjc-common/src/store"
import { Teacher } from 'mjc-common/src/components/Welcome';



const registrationCustom:FunctionComponent = (api:any ) => {



   const registrations = api.resources.find(({name}:any) => 'registrations' === name);
   // const isLogged = useStoreActions(actions => actions.RegistrationData.getTeacher)
   // isLogged({})
    
    // const teacherData = useStoreState(state => state.RegistrationData.teacherData)
       studentName(registrations)
       teacherName(registrations)
   
      //@ts-ignore
  // durationInput(registrations)
    specialityName(registrations)
   // dateTimeInput(registrations)
    return <> 
  
    
    </>

    

}

  //@ts-ignore
const durationInput = (registrations) => {
    //@ts-ignore
    const duration = registrations.fields.find(({f})  =>  'duration' === f.name);
    duration.input  =  ()  => (
    <SelectInput  source="duration"  choices={[
        { id: 1800, name: '30 minutes' },
        { id: 2700, name: '45 minutes' },
        { id: 3600, name: '1h' },
        { id: 5400, name: '1h30' },
        { id: 7200, name: '2h' },
    ]}  />
    );

    duration.input.defaultProps = {
    addField: true,
    addLabel: false
    };
}

const dateTimeInput = (registrations:any) => 
{
    const firstLesson = registrations.fields.find(({f}:any ) =>  'firstLessonAt' === f.name);

    firstLesson.input  =  ()  => (
        <DateTimeInput source="firstLessonAt" label="First lesson at:" type="datetime-local" options={{ampm: false}}/>
);

firstLesson.input.defaultProps = {
    addfield: 'true',
    addLabel: false
    };
}

// TODO: show only users with ROLE_STUDENT
const studentName = (registrations:any) => {

    //get student list from store at connexion 
    const studentData = store.getState().RegistrationData.studentData
    //Display users by name  instead id
    const registrUsers = registrations.fields.find(({ name }:any) => 'students' === name);

    // Set the field in the list and the show views
    registrUsers.field = (props:any) => (
    <ReferenceArrayField source={registrUsers.name} reference={registrUsers.reference.name} key={registrUsers.name} {...props}>
    <SingleFieldList>
        <FunctionField label="Name" render={(record:any) => `${ ' | ' } ${record.firstname} ${record.lastname} ${ ' | ' }`} />
    </SingleFieldList>
    </ReferenceArrayField>
    );
    const optionRenderer = (choice:any) => `${choice}`;
    // Set the input in the edit and create views
    registrUsers.input = (props:any) => (
    <ReferenceArrayInput source={registrUsers.name} reference={registrUsers.reference.name} label="Students" key={studentData} {...props} allowEmpty>
    <SelectArrayInput source='students' choices={studentData}/>
    </ReferenceArrayInput>
    );
}

// TODO: show only users with ROLE_TEACHER
const teacherName  = (registrations: any) => {
    //Get Teacher list at connexion
    const teacherData = store.getState().RegistrationData.teacherData
    const registrTeacher = registrations.fields.find(({ name }:any) => 'teacher' === name);

  
    const optionRenderer = (choice:any) => `${choice}`;
        registrTeacher.input = () => (
                <SelectInput source='teacher' choices={teacherData} />
            );


            registrTeacher.input.defaultProps = {
                addfield: 'true',
                addLabel: false
                };

 
     
}


const specialityName = (registrations:any) => {
        //Display speciality by name  instead id
    const specRegistration = registrations.fields.find(({ name }:any) => 'specialty' === name);
    // Set the input in the edit and create views
    specRegistration.input = (props:any) => (
    <ReferenceInput source={specRegistration.name} reference={specRegistration.reference.name} label="Specialties" key={specRegistration.name} {...props} allowEmpty>
    <SelectInput optionText="name"/>
    </ReferenceInput>
    );

    specRegistration.input.defaultProps = {
    addfield: 'true',
    addLabel: false
    };
}

export default registrationCustom;