import React from 'react';
import { SelectInput, ReferenceInput, SelectArrayInput,SimpleFormIterator, TextInput, ReferenceArrayInput,ReferenceArrayField,SingleFieldList, FunctionField, FormDataConsumer } from 'react-admin';
import {  DateTimeInput } from 'react-admin-date-inputs';
import { store } from "mjc-common/src/store"



const registrationCustom = (api) => {
    const registrations = api.resources.find(({ name }) =>  'registrations' === name);
    durationInput(registrations)
    studentName(registrations)
    specialityName(registrations)
    dateTimeInput(registrations)
    teacherName(registrations)

}

const durationInput = (registrations) => {
    const duration = registrations.fields.find(f  =>  'duration' === f.name);

    duration.input  =  props  => (
    <SelectInput  source="duration"  choices={[
        { id: 1800, name: '30 minutes' },
        { id: 2700, name: '45 minutes' },
        { id: 3600, name: '1h' },
        { id: 5400, name: '1h30' },
        { id: 7200, name: '2h' },
    ]}  />
    );

    duration.input.defaultProps = {
    addField: true,
    addLabel: false
    };
}

const dateTimeInput = (registrations) => 
{
    const firstLesson = registrations.fields.find(f  =>  'firstLessonAt' === f.name);

    firstLesson.input  =  props  => (
        <DateTimeInput source="firstLessonAt" label="First lesson at:" type="datetime-local" options={{ampm: false}}/>
);

firstLesson.input.defaultProps = {
    addField: true,
    addLabel: false
    };
}

const studentName = (registrations) => {

    //get student list from store at connexion 
    const studentData = store.getState().RegistrationData.studentData
    //Display users by name  instead id
    const registrUsers = registrations.fields.find(({ name }) => 'students' === name);

    // Set the field in the list and the show views
    registrUsers.field = (props) => (
    <ReferenceArrayField source={registrUsers.name} reference={registrUsers.reference.name} key={registrUsers.name} {...props}>
    <SingleFieldList>
        <FunctionField label="Name" render={(record) => `${ ' | ' } ${record.firstname} ${record.lastname} ${ ' | ' }`} />
    </SingleFieldList>
    </ReferenceArrayField>
    );
    const optionRenderer = (choice) => `${choice}`;
    // Set the input in the edit and create views
    registrUsers.input = (props) => (
    <ReferenceArrayInput source={registrUsers.name} reference={registrUsers.reference.name} label="Students" key={studentData} {...props} allowEmpty>
    <SelectArrayInput source='students' choices={studentData}/>
    </ReferenceArrayInput>
    );
}

const teacherName  = (registrations) => {
    //Get Teacher list at connexion
    const teacherData = store.getState().RegistrationData.teacherData
    const registrTeacher = registrations.fields.find(({ name }) => 'teacher' === name);

  
    const optionRenderer = (choice) => `${choice}`;
        registrTeacher.input = () => (
                <SelectInput source='teacher' choices={teacherData} />
            );


            registrTeacher.input.defaultProps = {
                addfield: 'true',
                addLabel: false
                };

 
     
}



const specialityName = (registrations) => {
        //Display speciality by name  instead id
    const specRegistration = registrations.fields.find(({ name }) => 'specialty' === name);
    // Set the input in the edit and create views
    specRegistration.input = props => (
    <ReferenceInput source={specRegistration.name} reference={specRegistration.reference.name} label="Specialties" key={specRegistration.name} {...props} allowEmpty>
    <SelectInput optionText="name"/>
    </ReferenceInput>
    );

    specRegistration.input.defaultProps = {
    addfield: "true",
    addLabel: false
    };
}

export default registrationCustom
