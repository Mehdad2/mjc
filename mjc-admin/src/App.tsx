import React, { Component } from 'react';
import RouteMatchRole from 'mjc-common/src/components/User/RouteRole'
import { Router, Switch } from 'react-router-dom'
import AppAdmin from './components/dashboard/index'
import ActionGetTeacher from './components/registrations/specificList'
import cookies from 'browser-cookies';
import history from 'mjc-common/src/config/history'
const cookie = cookies.get('tok')


class App extends Component {
  
  render() {
    return (
        <Router history={history}>
           <div>
            <RouteMatchRole/>
            <AppAdmin/>
          </div>
        </Router>
    );
  }
}
export default App;