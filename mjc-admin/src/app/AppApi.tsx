import React, { FunctionComponent } from 'react';
import { HydraAdmin } from '@api-platform/admin';
// eslint-disable-next-line
import specialtiesCustom from '../components/specialties'
import Dashboard from '../components/dashboard'
import usersCustom from '../components/users'
import  configurationsCustom from '../components/configurations'
import registrationCustom from '../components/registrations/index'
import parseHydraDocumentation from '@api-platform/api-doc-parser/lib/hydra/parseHydraDocumentation';
import Theme1 from '../Theme'
import login from 'mjc-common/src/components/Form/loginForm'
//import logout from 'mjc-common/src/Services/ClearStore'
import { AUTH_LOGIN, AUTH_LOGOUT } from 'react-admin';
import cookies from 'browser-cookies'
const ENTRYPOINT = 'http://localhost:8000/api';
import { useStoreState, useStoreActions } from "mjc-common/src/store"


const myApiDocumentationParser: FunctionComponent = (ENTRYPOINT) => parseHydraDocumentation(ENTRYPOINT)
  .then( (   {api}:any ) => {
    
    
/******************************************************************
 *                          SPECIALTIES 
 ******************************************************************/
    // Custom specialty boolean

    //specialtiesCustom(api)
  
/******************************************************************
 *                          USERS 
 ******************************************************************/

    // Add ArrayInput instead InputText by Default for roles (User)
    usersCustom(api)

/******************************************************************
 *                          REGISTRATIONS 
 ******************************************************************/    
    // Display choice list for duration of a lesson in Registration
    registrationCustom(api)
/******************************************************************
 *                          CONFIGURATIONS 
 ******************************************************************/  
    //configurationsCustom(api)

    
    return { api };
  })
;
const logout = (type: any) =>{
  if(type== AUTH_LOGOUT)
 cookies.erase('tok')

}

export default (props: any) =>    


 <HydraAdmin  dashboard={Dashboard} loginPage={login} apiDocumentationParser={myApiDocumentationParser}  authProvider={(AUTH_LOGOUT:any) =>{logout(AUTH_LOGOUT)}} entrypoint={ENTRYPOINT} theme={Theme1}/> ;



