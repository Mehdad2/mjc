import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { StoreProvider } from 'easy-peasy'
import {store, persistor} from 'mjc-common/src/store'
import { PersistGate } from "redux-persist/integration/react";


ReactDOM.render(
  <PersistGate loading={<div>Loading</div>} persistor={persistor}>
    <StoreProvider store={store}>
        <App/>
    </StoreProvider>
  </PersistGate>,
    document.getElementById('root')
  )

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

