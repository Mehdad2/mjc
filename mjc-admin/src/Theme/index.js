import { createMuiTheme } from '@material-ui/core/styles';


const Theme1 = createMuiTheme({
    typography: {
        useNextVariants: true,
      },
      palette: {
        primary: {
          light: '#F08080',
          main: '#7986cb',
          dark: '#283593',
          contrastText: '#FFF',
        },
      },
      shape: {
        borderRadius: 8
      },
    overrides: {
        MuiAvatar: {
            src:'./src/public/images/planneur2.png'

        },
        MuiDrawer: {
            docked: {
            background: '#18202c',
            // this is where magic happens
            '& *': { color: 'rgba(255, 255, 255, 0.7)' },
          },
        },
      },
    });


export default Theme1