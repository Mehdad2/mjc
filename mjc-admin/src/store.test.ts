import { createStore, createTypedHooks, EasyPeasyConfig } from "easy-peasy";
import model, { StoreModel } from "mjc-common/src/model";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import Encrypt from 'mjc-common/src/Services/encryptor'

const { useStoreActions, useStoreState, useStoreDispatch } = createTypedHooks<StoreModel>();

export { useStoreActions, useStoreState, useStoreDispatch };




//@ts-ignore
const store = createStore(model, {
    reducerEnhancer: reducer =>
      persistReducer(
        {
          key: "easypeasystate",
          storage,
          whitelist: ['UserData', 'RegistrationData'],
          //transforms: [Encrypt.encryptorStore]
        },
        reducer
      )
  });

const persistor = persistStore(store);
export {store, persistor};