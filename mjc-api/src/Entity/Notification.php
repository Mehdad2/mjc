<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isLessonCanceled;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $message;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="notificationSend", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lesson", inversedBy="notifications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lesson;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NotificationReceiver", mappedBy="notification")
     */
    private $notificationReceivers;

    public function __construct()
    {
        $this->notificationReceivers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsLessonCanceled(): ?bool
    {
        return $this->isLessonCanceled;
    }

    public function setIsLessonCanceled(bool $isLessonCanceled): self
    {
        $this->isLessonCanceled = $isLessonCanceled;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSender(): ?User
    {
        return $this->sender;
    }

    public function setSender(User $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getLesson(): ?Lesson
    {
        return $this->lesson;
    }

    public function setLesson(?Lesson $lesson): self
    {
        $this->lesson = $lesson;

        return $this;
    }

    /**
     * @return Collection|NotificationReceiver[]
     */
    public function getNotificationReceivers(): Collection
    {
        return $this->notificationReceivers;
    }

    public function addNotificationReceiver(NotificationReceiver $notificationReceiver): self
    {
        if (!$this->notificationReceivers->contains($notificationReceiver)) {
            $this->notificationReceivers[] = $notificationReceiver;
            $notificationReceiver->setNotification($this);
        }

        return $this;
    }

    public function removeNotificationReceiver(NotificationReceiver $notificationReceiver): self
    {
        if ($this->notificationReceivers->contains($notificationReceiver)) {
            $this->notificationReceivers->removeElement($notificationReceiver);
            // set the owning side to null (unless already changed)
            if ($notificationReceiver->getNotification() === $this) {
                $notificationReceiver->setNotification(null);
            }
        }

        return $this;
    }
}
