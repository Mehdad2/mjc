<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; 


/**
 * @ApiResource(attributes={"normalization_context"={"groups"={"configuration"}}})
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationRepository")
 * 
 */
class Configuration implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"configuration"})
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"configuration"})
     * @Assert\NotBlank
     */
    private $openAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"configuration"})
     * @Assert\NotBlank
     */
    private $closeAt;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     * @Groups({"configuration"})
     */
    private $isMonday;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     * @Groups({"configuration"})
     */
    private $isTuesday;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     * @Groups({"configuration"})
     * 
     */
    private $isWednesday;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     * @Groups({"configuration"})
     * 
     */
    private $isThursday;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     * @Groups({"configuration"})
     */
    private $isFriday;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     * @Groups({"configuration"})
     * 
     */
    private $isSaturday;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     * @Groups({"configuration"})
     */
    private $isSunday;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=1)
     * @Groups({"configuration"})
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 1,
     *      max = 1,
     *      minMessage = "The Zone consists of only {{ limit }} character : A, B or C",
     *      maxMessage = "The Zone consists of only {{ limit }} character : A, B or C"
     * )
     */
    private $zone;
   
    public function __construct()
    {
        $this->updateDatetime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOpenAt(): ?\DateTimeInterface
    {
        return $this->openAt;
    }

    public function setOpenAt(\DateTimeInterface $openAt): self
    {
        $this->openAt = $openAt;

        return $this;
    }

    public function getCloseAt(): ?\DateTimeInterface
    {
        return $this->closeAt;
    }

    public function setCloseAt(\DateTimeInterface $closeAt): self
    {
        $this->closeAt = $closeAt;

        return $this;
    }

    public function getIsMonday(): ?bool
    {
        return $this->isMonday;
    }

    public function setIsMonday(bool $isMonday): self
    {
        $this->isMonday = $isMonday;

        return $this;
    }

    public function getIsTuesday(): ?bool
    {
        return $this->isTuesday;
    }

    public function setIsTuesday(bool $isTuesday): self
    {
        $this->isTuesday = $isTuesday;

        return $this;
    }

    public function getIsWednesday(): ?bool
    {
        return $this->isWednesday;
    }

    public function setIsWednesday(bool $isWednesday): self
    {
        $this->isWednesday = $isWednesday;

        return $this;
    }

    public function getIsThursday(): ?bool
    {
        return $this->isThursday;
    }

    public function setIsThursday(bool $isThursday): self
    {
        $this->isThursday = $isThursday;

        return $this;
    }

    public function getIsFriday(): ?bool
    {
        return $this->isFriday;
    }

    public function setIsFriday(bool $isFriday): self
    {
        $this->isFriday = $isFriday;

        return $this;
    }

    public function getIsSaturday(): ?bool
    {
        return $this->isSaturday;
    }

    public function setIsSaturday(bool $isSaturday): self
    {
        $this->isSaturday = $isSaturday;

        return $this;
    }

    public function getIsSunday(): ?bool
    {
        return $this->isSunday;
    }

    public function setIsSunday(bool $isSunday): self
    {
        $this->isSunday = $isSunday;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getZone(): ?string
    {
        return $this->zone;
    }

    public function setZone(string $zone): self
    {
        $this->zone = $zone;

        return $this;
    }
    
    /**
    * @ORM\PrePersist()
    * @ORM\PreUpdate()
    */
    public function updateDatetime() : void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

          /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->name,
            $this->openAt,
            $this->closeAt,
            $this->isMonday,
            $this->isTuesday,
            $this->isWednesday,
            $this->isThursday,
            $this->isFriday,
            $this->isSaturday,
            $this->isSunday,
            $this->createdAt,
            $this->updatedAt,
            $this->zone
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->name,
            $this->openAt,
            $this->closeAt,
            $this->isMonday,
            $this->isTuesday,
            $this->isWednesday,
            $this->isThursday,
            $this->isFriday,
            $this->isSaturday,
            $this->isSunday,
            $this->createdAt,
            $this->updatedAt,
            $this->zone
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
}
