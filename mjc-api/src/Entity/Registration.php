<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; 
use Doctrine\Common\Collections\Criteria;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(attributes={"normalization_context"={"groups"={"registration_create"}}}, cacheHeaders={"max_age"=120, "shared_max_age"=360})
 * @ORM\Entity(repositoryClass="App\Repository\RegistrationRepository")
 * itemOperations={
 *  "post"={"route_name"="api_registrations_post"}
 *  }
 *
 */
class Registration
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

      /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Specialty", inversedBy="registrations",  cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"registration_create"})
     */
    private $specialty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"registration_create"})
     */
    private $room;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"registration_create"})
     * @Assert\NotBlank
     */
    private $duration;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"registration_create"})
     * @Assert\NotBlank
     */
    private $firstLessonAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", unique=false)
     * @Groups({"registration_create"})
     * @Assert\NotBlank
     */
    private $students;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id", unique=false)
     * @Groups({"registration_create"})
     * @Assert\NotBlank
     */
    private $teacher;

  
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lesson", mappedBy="registration")
     * 
     */
    private $lessons;



    public function __construct()
    {
        $this->updateDatetime();
        $this->lessons = new ArrayCollection();
        $this->students = new ArrayCollection();
      
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoom(): ?string
    {
        return $this->room;
    }

    public function setRoom(?string $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getFirstLessonAt(): ?\DateTimeInterface
    {
        return $this->firstLessonAt;
    }

    public function setFirstLessonAt(\DateTimeInterface $firstLessonAt): self
    {
        $firstLessonAt->modify('+2 hours');
        $this->firstLessonAt = $firstLessonAt;
        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

  

    // Lessons
    /**
     * @return Collection|Lesson[]
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function addLesson(Lesson $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons[] = $lesson;
        }

        return $this;
    }

    public function removeLesson(Lesson $lesson): self
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
        }

        return $this;
    }

    // Fin lesssons

    public function getSpecialty(): ?Specialty
    {
        return $this->specialty;
    }

    public function setSpecialty(?Specialty $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }
      /**
    * @ORM\PrePersist()
    * @ORM\PreUpdate()
    */
    public function updateDatetime() : void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * @return Collection|User[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(): Collection
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
        }

        return $this;
    }

    public function removeStudent(User $student): self
    {
        if ($this->students->contains($student)) {
            $this->students->removeElement($student);
        }

        return $this;
    }

    public function getTeacher(): ?User
    {
        return $this->teacher;
    }

    public function setTeacher(?User $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }
    
}
