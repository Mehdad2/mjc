<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert; 

/**
 * 
 * 
 * @ApiResource(attributes={"normalization_context"={"groups"={"user_get"}}})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * itemOperations={
 * "get"
 *      "put"={"route_name"="api_users_put"}
 *  }, collectionOperations={
 * "get"
 * 
 *      "post"={"route_name"="api_users_post"}
 *  }
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     * @Groups({"user_get"})
     * @Assert\NotBlank
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=80)
     * @Groups({"user_get"})
     * @Assert\NotBlank
     */
    private $lastname;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_get"})
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Groups({"user_get"})
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"user_get"})
     */
    private $streetNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_get"})
     */
    private $streetName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"user_get"})
     * @Assert\Length(
     *      min = 5,
     *      max = 5,
     *      minMessage = "The Zone consists of only {{ limit }} character : A, B or C",
     *      maxMessage = "The Zone consists of only {{ limit }} character : A, B or C"
     * )
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_get"})
     */
    private $city;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;


    /**
     * @ORM\Column(type="boolean",options={"default"=0})
     * @Groups({"user_get"})
     */
    private $isActive;


    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user_get"})
     * @Assert\NotBlank
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"user_get"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"user_get"})
     */
    private $password;

    protected $encoderpass;

   

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Notification", mappedBy="sender", cascade={"persist", "remove"})
     */
    private $notificationSend;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NotificationReceiver", mappedBy="receiver")
     */
    private $notificationReceivers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Email", mappedBy="user", cascade={"persist"})
     */
    private $emailId;
    


    public function __construct()
    {     
        $this->updateDatetime();
        $this->notificationReceivers = new ArrayCollection();
        $this->emailId = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        //$roles[];

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }




    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getStreetNumber(): ?int
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(?int $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(?string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getZipCode(): ?int
    {
        return $this->zipCode;
    }

    public function setZipCode(?int $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

     /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateDatetime() : void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

   
    public function getIsActive(): bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

      /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->lasname,
            $this->firstname,
            $this->password,
            $this->createdAt,
            $this->updatedAt,
            $this->birthDate
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->lasname,
            $this->firstname,
            $this->password,
            $this->createdAt,
            $this->updatedAt,
            $this->birthDate
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }

    public function __toString()
    {
        return $this->firstname.' '.$this->lastname;
    }


    public function getNotificationSend(): ?Notification
    {
        return $this->notificationSend;
    }

    public function setNotificationSend(Notification $notificationSend): self
    {
        $this->notificationSend = $notificationSend;

        // set the owning side of the relation if necessary
        if ($this !== $notificationSend->getSender()) {
            $notificationSend->setSender($this);
        }

        return $this;
    }

    /**
     * @return Collection|NotificationReceiver[]
     */
    public function getNotificationReceivers(): Collection
    {
        return $this->notificationReceivers;
    }

    public function addNotificationReceiver(NotificationReceiver $notificationReceiver): self
    {
        if (!$this->notificationReceivers->contains($notificationReceiver)) {
            $this->notificationReceivers[] = $notificationReceiver;
            $notificationReceiver->setReceiver($this);
        }
        return $this;
    }

    public function removeNotificationReceiver(NotificationReceiver $notificationReceiver): self
    {
        if ($this->notificationReceivers->contains($notificationReceiver)) {
            $this->notificationReceivers->removeElement($notificationReceiver);
            // set the owning side to null (unless already changed)
            if ($notificationReceiver->getReceiver() === $this) {
                $notificationReceiver->setReceiver(null);
            }
        }

        return $this;
    }
    
    /**
     * @return Collection|Email[]
     */
    public function getEmailId(): Collection
    {
        return $this->emailId;
    }

    public function addEmailId(Email $emailId): self
    {
        if (!$this->emailId->contains($emailId)) {
            $this->emailId[] = $emailId;
            $emailId->setUser($this);
        }

        return $this;
    }

 
    public function removeEmailId(Email $emailId): self
    {
        if ($this->emailId->contains($emailId)) {
            $this->emailId->removeElement($emailId);
            // set the owning side to null (unless already changed)
            if ($emailId->getUser() === $this) {
                $emailId->setUser(null);
            }
        }

        return $this;
    }

}
