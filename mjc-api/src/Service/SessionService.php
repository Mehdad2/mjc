<?php
namespace App\Service;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
 
class SessionService {
 
  /**
   * L'objet unique SessionService
   *
   * @var SessionService
   * @access private
   * @static
   */
   private static $_instance = null;
 


    /**
    * @param SessionInterface $session
    */
   private $session;

   /**
    * Constructeur de la classe
    *
    * @param string $name
    * @param string $pvalue
    * @return void
    * @access private
    */
   private function __construct(SessionInterface $session) {
 
     $this->session = $session;
   }
 
   /**
    * Méthode qui crée l'unique instance de la classe
    * si elle n'existe pas encore puis la retourne.
    *
    * @param string $name
    * @param string $value
    * @return SessionService
    */
   public static function getInstance() {
 
     if(is_null(self::$_instance)) {
       self::$_instance = new SessionService($name, $value);  
     }
 
     return self::$_instance;
   }
 


  public function getSession($value) {
      $this->session->get($value);
  }
}
 
?>