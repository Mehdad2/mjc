<?php
namespace App\Service;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class UserService extends Controller
{
    public function __construct(TokenStorageInterface $tokenStorageInterface, JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

    /** @var  TokenStorageInterface */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface  $storage
     */
    /*public function __construct(
        TokenStorageInterface $storage
    )
    {
        $this->tokenStorage = $storage;
    }*/

    /**
     * Adds additional data to the generated JWT
     *
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
  /*  public function onJWTCreated(JWTCreatedEvent $event)
    {
        /** @var $user \App\Entity\User */
       /* $user = $event->getUser();

        // merge with existing event data
        $payload = array_merge(
            $event->getData(),
            [
                'userId' => $user->getId()
            ]
        );

        $event->setData($payload);
    } */
    

    public function getCurrentUser()
    {
        $token = $this->tokenStorageInterface->getToken();
        if ($token instanceof TokenInterface) {

            /** @var User $user */
           $user = $token->getUser();
            return $user;

        } else {
            return null;
        }
    } 
    


    public function getUser() {
       
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorageInterface->getToken());

        return $decodedJwtToken;
    }


 
}