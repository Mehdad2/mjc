<?php

namespace App\Service;


// use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class ServiceHolidays {
  // TODO: Remplacer $weekend par tous les jours de la semaines dans la config = false
  // Récupérer un tableau avec les jours false
  public static function estFerie($timestamp, $zone, $daysNotWorked) {
   // Initialisation de tous les jours de la semaine à true par défaut
  $monday= true;
  $tuesday= true;
  $wednesday= true;
  $thursday= true;
  $friday= true;
  $saturday= true;
  $sunday = true;
   
   // PAsser les $daysNotWorked à false
   foreach ($daysNotWorked as $dayIsFalse) {
    // dump($dayIsFalse);
    switch ($dayIsFalse) {
        case "isMonday" : $monday = 0; break;
        case "isTuesday" : $tuesday = 0; break;
        case "isWednesday" : $wednesday = 0; break;
        case "isThursday" : $thursday = 0; break;
        case "isFriday" : $friday = 0; break;
        case "isSaturday" : $saturday = 0; break;
        case "isSunday" : $sunday = 0; break;
    }
}

    // Initialisation de la date de début
    $jour = date("d", $timestamp);
    $mois = date("m", $timestamp);
    $annee = date("Y", $timestamp);

    // Pont de l'Ascension annee_scolaire: "2019-2020"
    $debutPont = 1590019140;
    $finPont = 1590364801;
    // Toussaint "annee_scolaire": "2019-2020"
    $debutToussaint = 1571529599;
    $finToussain = 1572825601;
    // Noël "annee_scolaire": "2019-2020"
    $debutNoel = 1576972799;
    $finNoel = 1578268801;

    // Switch zones pour dates de début et fin des vacances hiver et printemps annee_scolaire: "2019-2020"
    switch ($zone) {
      case "A":
        $debutHiver = 1582354800;
        $finHiver = 1583708400;
        $debutPrintemps = 1587193200;
        $finPrintemps = 1588546800;
        break;
      case "B":
        $debutHiver = 1581811199;
        $finHiver = 1583107201;
        $debutPrintemps = 1586649599;
        $finPrintemps = 1587945601;
        break;
      case "C":
        $debutHiver = 1581206399;
        $finHiver = 1582502400;
        $debutPrintemps = 1586044799;
        $finPrintemps = 1587340801;
        break;
    }
	 

    // Vérification parmis les jours férié fixes
    if (($jour == 1 && $mois == 1) || //  1er Janvier 	(Jour de l'an)
      ($jour == 1 && $mois == 5) || //  1er Mai 		(Fête du travail)
      ($jour == 8 && $mois == 5) || //  8   Mai 		(Fête de la victoire - Fin de la seconde guerre mondiale)
      ($jour == 14 && $mois == 7) || // 14   Juillet 	(Fête nationale)
      ($jour == 15 && $mois == 8) || // 15   Aout 	(Assomption)
      ($jour == 1 && $mois == 11) || //  1er Novembre 	(Toussain)
      ($jour == 11 && $mois == 11) || // 11   Novembre 	(Armistice 1918)
      ($jour == 25 && $mois == 12)) // 25   Décembre 	(Noël)
    {
      return true;
    }

    $jour_julien = unixtojd($timestamp);
    $jour_semaine = jddayofweek($jour_julien, 0);


    // Tester si les jours de la semaines sont considérés comme travaillés ou non dans la config
    // Retourne true si chômé
    if($monday === false) {
      if ($jour_semaine == 1) {
        echo "lundi";
        return true;
      }
    }

    if($tuesday === false) {
      if ($jour_semaine == 2) {
        echo "mardi";
        return true;
      }
    }
    if($wednesday === false) {
      if ($jour_semaine == 3) {
        echo "mercredi";
        return true;
      }
    }
    if($thursday === false) {
      if ($jour_semaine == 4) {
        echo "jeudi";
        return true;
      }
    }
    if($friday === false) {
      if ($jour_semaine == 5) {
        echo "vendredi";
        return true;
      }
    }
    if($saturday === false) {
      if ($jour_semaine == 6) {
        echo "samedi";
        return true;
      }
    }
    if($sunday === false) {
      if ($jour_semaine == 0) {
        echo "dimanche";
        return true;
      }
    }
    // Calcul du jour de pâques
    $date_paques = easter_date($annee);
    $jour_paques = date("d", $date_paques);
    $mois_paques = date("m", $date_paques);

    // Si le jour testé est le jour de paques
    if ($jour_paques == $jour && $mois_paques == $mois) {
      return true;
    }

    // Calcul du jour du lundi de pâques
    $date_lundi_paques = mktime(date("H", $date_paques),
      date("i", $date_paques),
      date("s", $date_paques),
      date("m", $date_paques),
      date("d", $date_paques) + 1,
      date("Y", $date_paques));
    $jour_lundi_paques = date("d", $date_lundi_paques);
    $mois_lundi_paques = date("m", $date_lundi_paques);

    // Si le jour testé est le jour de paques
    if ($jour_lundi_paques == $jour && $mois_lundi_paques == $mois) {
      return true;
    }

    // Calcul du jour de l ascension (39 jours après Paques)
    $date_ascension = mktime(date("H", $date_paques),
      date("i", $date_paques),
      date("s", $date_paques),
      date("m", $date_paques),
      date("d", $date_paques) + 39,
      date("Y", $date_paques));
    $jour_ascension = date("d", $date_ascension);
    $mois_ascension = date("m", $date_ascension);

    // Si le jour testé est le jour de l'ascension
    if ($jour_ascension == $jour && $mois_ascension == $mois) {
      return true;
    }

    // Calcul de Pentecôte (7 semaines après Paques)
    $date_pentecote = mktime(date("H", $date_paques),
      date("i", $date_paques),
      date("s", $date_paques),
      date("m", $date_paques),
      date("d", $date_paques) + 49,
      date("Y", $date_paques));
    $jour_pentecote = date("d", $date_pentecote);
    $mois_pentecote = date("m", $date_pentecote);

    // Si le jour testé est le jour de la pentecote
    if ($jour_pentecote == $jour && $mois_pentecote == $mois) {
      return true;
    }

    // Calcul du lundi de Pentecôte (Lendemain de paques)
    $date_lundi_pentecote = mktime(date("H", $date_pentecote),
      date("i", $date_pentecote),
      date("s", $date_pentecote),
      date("m", $date_pentecote),
      date("d", $date_pentecote) + 1,
      date("Y", $date_pentecote));
    $jour_lundi_pentecote = date("d", $date_lundi_pentecote);
    $mois_lundi_pentecote = date("m", $date_lundi_pentecote);

    // Si le jour testé est le jour de la pentecote
    if ($jour_lundi_pentecote == $jour && $mois_lundi_pentecote == $mois) {
      return true;
    }

   
    //Si le jour testé est compris entre la date de début et la date de fin de la Toussaint
    if ($timestamp > $debutToussaint && $timestamp < $finToussain) {
      // echo "Vavances de la Toussaint en Zone A, B et C";
      return true;
    }

    // Si le jour testé est compris entre la date de début et la date de fin des vacances de Noël
    //@TODO check timestamp pour les vacs ici: année 2018 et il faut année 2019 /2020
    if ($timestamp > $debutNoel && $timestamp < $finNoel) {
      // echo "Vavances de Noël en Zone A, B et C";
      return true;
    }

    // Si le jour testé est compris entre la date de début et la date de fin des vacances d'hiver
    if ($timestamp > $debutHiver && $timestamp < $finHiver) {
      // echo "Vavances d'hiver en Zone C";
      return true;
    }

    // Si le jour testé est compris entre la date de début et la date de fin des vacances d'hiver
    if ($timestamp > $debutPrintemps && $timestamp < $finPrintemps) {
      // echo "Vavances de printemps en Zone C";
      return true;
    }

     // Si le jour testé est compris entre la date de début et la date de fin du pont de l'ascension
     if ($timestamp > $debutPont && $timestamp < $finPont) {
      // echo "Pont de l'ascensions en Zone A, B et C";
      return true;
    }
    // Si l'execution est parvenue jusque là, c'est que le jour transmis n'est pas férié
    return false;

  }

  public static function holidays($timestamp, $zone) {
    // Initialisation de la date de début
    $jour = date("d", $timestamp);
    $mois = date("m", $timestamp);
    $annee = date("Y", $timestamp);

    // Pont de l'Ascension annee_scolaire: "2019-2020"
    $debutPont = 1590019140;
    $finPont = 1590364801;
    // Toussaint "annee_scolaire": "2019-2020"
    $debutToussaint = 1571529599;
    $finToussain = 1572825601;
    // Noël "annee_scolaire": "2019-2020"
    $debutNoel = 1576972799;
    $finNoel = 1578268801;

    // Switch zones pour dates de début et fin des vacances hiver et printemps annee_scolaire: "2019-2020"
    switch ($zone) {
      case "A":
        $debutHiver = 1582354800;
        $finHiver = 1583708400;
        $debutPrintemps = 1587193200;
        $finPrintemps = 1588546800;
        break;
      case "B":
        $debutHiver = 1581811199;
        $finHiver = 1583107201;
        $debutPrintemps = 1586649599;
        $finPrintemps = 1587945601;
        break;
      case "C":
        $debutHiver = 1581206399;
        $finHiver = 1582502400;
        $debutPrintemps = 1586044799;
        $finPrintemps = 1587340801;
        break;
    }
	 

    // Vérification parmis les jours férié fixes
    if (($jour == 1 && $mois == 1) || //  1er Janvier 	(Jour de l'an)
      ($jour == 1 && $mois == 5) || //  1er Mai 		(Fête du travail)
      ($jour == 8 && $mois == 5) || //  8   Mai 		(Fête de la victoire - Fin de la seconde guerre mondiale)
      ($jour == 14 && $mois == 7) || // 14   Juillet 	(Fête nationale)
      ($jour == 15 && $mois == 8) || // 15   Aout 	(Assomption)
      ($jour == 1 && $mois == 11) || //  1er Novembre 	(Toussain)
      ($jour == 11 && $mois == 11) || // 11   Novembre 	(Armistice 1918)
      ($jour == 25 && $mois == 12)) // 25   Décembre 	(Noël)
    {
      return true;
    }
      // Calcul du jour de pâques
      $date_paques = easter_date($annee);
      $jour_paques = date("d", $date_paques);
      $mois_paques = date("m", $date_paques);
  
      // Si le jour testé est le jour de paques
      if ($jour_paques == $jour && $mois_paques == $mois) {
        return true;
      }
  
      // Calcul du jour du lundi de pâques
      $date_lundi_paques = mktime(date("H", $date_paques),
        date("i", $date_paques),
        date("s", $date_paques),
        date("m", $date_paques),
        date("d", $date_paques) + 1,
        date("Y", $date_paques));
      $jour_lundi_paques = date("d", $date_lundi_paques);
      $mois_lundi_paques = date("m", $date_lundi_paques);
  
      // Si le jour testé est le jour de paques
      if ($jour_lundi_paques == $jour && $mois_lundi_paques == $mois) {
        return true;
      }
  
      // Calcul du jour de l ascension (39 jours après Paques)
      $date_ascension = mktime(date("H", $date_paques),
        date("i", $date_paques),
        date("s", $date_paques),
        date("m", $date_paques),
        date("d", $date_paques) + 39,
        date("Y", $date_paques));
      $jour_ascension = date("d", $date_ascension);
      $mois_ascension = date("m", $date_ascension);
  
      // Si le jour testé est le jour de l'ascension
      if ($jour_ascension == $jour && $mois_ascension == $mois) {
        return true;
      }
  
      // Calcul de Pentecôte (7 semaines après Paques)
      $date_pentecote = mktime(date("H", $date_paques),
        date("i", $date_paques),
        date("s", $date_paques),
        date("m", $date_paques),
        date("d", $date_paques) + 49,
        date("Y", $date_paques));
      $jour_pentecote = date("d", $date_pentecote);
      $mois_pentecote = date("m", $date_pentecote);
  
      // Si le jour testé est le jour de la pentecote
      if ($jour_pentecote == $jour && $mois_pentecote == $mois) {
        return true;
      }
  
      // Calcul du lundi de Pentecôte (Lendemain de paques)
      $date_lundi_pentecote = mktime(date("H", $date_pentecote),
        date("i", $date_pentecote),
        date("s", $date_pentecote),
        date("m", $date_pentecote),
        date("d", $date_pentecote) + 1,
        date("Y", $date_pentecote));
      $jour_lundi_pentecote = date("d", $date_lundi_pentecote);
      $mois_lundi_pentecote = date("m", $date_lundi_pentecote);
  
      // Si le jour testé est le jour de la pentecote
      if ($jour_lundi_pentecote == $jour && $mois_lundi_pentecote == $mois) {
        return true;
      }
  
     
      //Si le jour testé est compris entre la date de début et la date de fin de la Toussaint
      if ($timestamp > $debutToussaint && $timestamp < $finToussain) {
        // echo "Vavances de la Toussaint en Zone A, B et C";
        return true;
      }
      // var_dump('ça passe');
      // var_dump($timestamp);
  
      // Si le jour testé est compris entre la date de début et la date de fin des vacances de Noël
      //@todo $timestamp >= $debutNoel && $timestamp <= $finNoel (change to) $timestamp >= $debutNoel && $timestamp >= $finNoel
      if ($timestamp > $debutNoel && $timestamp < $finNoel) {
        // var_dump($timestamp . 'ça passe a noel');
        //echo "Vavances de Noël en Zone A, B et C";
        return true;
      }
  
      // Si le jour testé est compris entre la date de début et la date de fin des vacances d'hiver
      if ($timestamp > $debutHiver && $timestamp < $finHiver) {
        // echo "Vavances d'hiver en Zone C";
        return true;
      }
  
      // Si le jour testé est compris entre la date de début et la date de fin des vacances d'hiver
      if ($timestamp > $debutPrintemps && $timestamp < $finPrintemps) {
        // echo "Vavances de printemps en Zone C";
        return true;
      }
  
       // Si le jour testé est compris entre la date de début et la date de fin du pont de l'ascension
       if ($timestamp > $debutPont && $timestamp < $finPont) {
        // echo "Pont de l'ascensions en Zone A, B et C";
        return true;
      }
      // Si l'execution est parvenue jusque là, c'est que le jour transmis n'est pas férié
      return false;
  
  }
}
