<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190930124522 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE registration_user');
        $this->addSql('ALTER TABLE registration DROP FOREIGN KEY FK_62A8A7A741807E1D');
        $this->addSql('DROP INDEX IDX_62A8A7A741807E1D ON registration');
        $this->addSql('ALTER TABLE registration ADD user_id INT DEFAULT NULL, DROP teacher_id, CHANGE room room VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_62A8A7A7A76ED395 ON registration (user_id)');
        $this->addSql('ALTER TABLE user CHANGE birth_date birth_date DATETIME DEFAULT NULL, CHANGE phone_number phone_number VARCHAR(15) DEFAULT NULL, CHANGE street_number street_number INT DEFAULT NULL, CHANGE street_name street_name VARCHAR(255) DEFAULT NULL, CHANGE zip_code zip_code INT DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT NULL, CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE email CHANGE user_id user_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE registration_user (registration_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_42ADC195A76ED395 (user_id), INDEX IDX_42ADC195833D8F43 (registration_id), PRIMARY KEY(registration_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE registration_user ADD CONSTRAINT FK_42ADC195833D8F43 FOREIGN KEY (registration_id) REFERENCES registration (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE registration_user ADD CONSTRAINT FK_42ADC195A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE email CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE registration DROP FOREIGN KEY FK_62A8A7A7A76ED395');
        $this->addSql('DROP INDEX IDX_62A8A7A7A76ED395 ON registration');
        $this->addSql('ALTER TABLE registration ADD teacher_id INT DEFAULT NULL, DROP user_id, CHANGE room room VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A741807E1D FOREIGN KEY (teacher_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_62A8A7A741807E1D ON registration (teacher_id)');
        $this->addSql('ALTER TABLE user CHANGE birth_date birth_date DATETIME DEFAULT \'NULL\', CHANGE phone_number phone_number VARCHAR(15) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE street_number street_number INT DEFAULT NULL, CHANGE street_name street_name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE zip_code zip_code INT DEFAULT NULL, CHANGE city city VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
