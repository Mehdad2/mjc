<?php
namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\UserService;

class UserController extends AbstractController
{

     /**
     * @Route(
     *     name="api_users_post",
     *     path="/api/users",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=User::class,
     *         "_api_collection_operation_name"="post"
     *     }
     * )
     */
    public function postAction(User $data, UserPasswordEncoderInterface $encoder): User
    {
        return $this->encodePassword($data, $encoder);
    }

    /**
     * @Route(
     *     name="api_users_put",
     *     path="/api/users/{id}",
     *     requirements={"id"="\d+"},
     *     methods={"PUT"},
     *     defaults={
     *         "_api_resource_class"=User::class,
     *         "_api_item_operation_name"="put"
     *     }
     * )
     */
    public function putAction(User $data, UserPasswordEncoderInterface $encoder): User
    {
        return $this->encodePassword($data, $encoder);
    }

    protected function encodePassword(User $data, UserPasswordEncoderInterface $encoder): User
    {
        $encoded = $encoder->encodePassword($data, $data->getPassword());
        $data->setPassword($encoded);

        return $data;
    }

    /**
     * @Route(
     * name="api_user",
     * path="/api/user",
     * methods={"GET"}
     * 
     * )
     */

     public function getDataUser(UserService $service, EntityManagerInterface $em) {

        $email = $service->getCurrentUser()->getUsername();

        $userLogin = $em->getRepository(User::class)->findOneBy(['email' => $email]);
        $userData = ['role' =>  $userLogin->getRoles(), 'email' =>  $userLogin->getEmail()];

        return new JsonResponse(
            $userData
        );

     }


}