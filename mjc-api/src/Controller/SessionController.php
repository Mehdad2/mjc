<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\MigratingSessionHandler;


class SessionController extends AbstractController
{
    private $service;

    public function __construct(SessionInterface  $session)
    {
        $this->session = $session;
      
    } 
   
    /**
     * @Route(
     * name="api_session_check",
     * path="/api/session/check",
     * methods={"POST"}
     * )
     */
   
    public function sessionCheck(Request $request, \SessionHandlerInterface $currentHandler)
    {

        try {
            $data = $request->getContent();
            $encodedData = json_decode($data);
            $tokenFormat = substr($encodedData->token,0,11);
            $emailFormat = substr($encodedData->email,0,1);
            $xsrfRequest = $encodedData->xsrf;
            $tokenRequest = $encodedData->token;
            $this->session->start();
            $sessionStorage = new MigratingSessionHandler($currentHandler,$currentHandler);
            $sessionData = json_decode($sessionStorage->read($tokenFormat.$emailFormat));
            $tokenSession = $sessionData->token;
            $xsrfSession = $sessionData->xsrfToken;
            if($tokenSession === $tokenRequest &&  $xsrfSession === $xsrfRequest) {
                return new JsonResponse(true);
            } else {
                throw new \Error('invalid Token or xsrf');
            }
        }catch(\Exception $e) {
            return new JsonResponse($e->getMessage());
        }
        
    }

}