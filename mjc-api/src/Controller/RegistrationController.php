<?php
namespace App\Controller;

use App\Entity\Lesson;
use App\Entity\User;
use App\Entity\Registration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\ServiceHolidays;
use Symfony\Component\HttpFoundation\JsonResponse;

class RegistrationController extends AbstractController
{

     /**
     * @Route(
     *     name="api_registrations_post",
     *     path="/api/registrations",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=Registration::class,
     *         "_api_collection_operation_name"="post"
     *     }
     * )
     */
    public function postAction(Registration $data)
    {
        $em = $this->getDoctrine()->getManager();

        //TODO: remplacer la recherche de la config par une variable.
        $configName = "Louveciennes 2019-2020";
        $repo = $em->getRepository('App:Configuration');
        $days = $repo->findConfigurattionDays($configName);
        $goodConfig = $repo->findOneBy(['name' => $configName]);

        $zone = $goodConfig->getZone();
        $openAt = $goodConfig->getOpenAt();
        $closeAt = $goodConfig->getCloseAt();

        // Test if $firstAt is Holiday with a timestamp.
        $firstDate = $data->getFirstLessonAt();
        $firstDateTimestamp =  $firstDate->getTimestamp();

        $daysNotWorked = [];

        foreach($days as $key => $value) {
            foreach($value as $weekDay => $result) {
                if ($result === false) {
                    $daysNotWorked[] = $weekDay;
                }
            }
        }
        if ($firstDate < $openAt || $firstDate > $closeAt) {
            $message = "Vous ne pouvez pas enregistrer en dehors des jours de la config" ;
            dd($message);

        }
        // 1. Ne vérifier les jours nons travaillés qu'en premier : si le premier cours est un lundi,
        // forcément les autres aussi
        if (ServiceHolidays::estFerie($firstDateTimestamp, $zone, $daysNotWorked)) {
            $message = 'Attention : vous ne pouvez pas enregistrer une leçon pendant les vacances';
            dd($message);
            
        } else {
            //  Register first lesson
            $firstLesson = new Lesson;
            $firstLesson->setStartAt($firstDate);
            $firstLesson->setRegistration($data);
            $em->persist($firstLesson);
            $em->flush();
        
            $newDate = $firstLesson->getStartAt();
            while ($newDate <= $closeAt) {
                $newDate->modify('+7 day');
                $timestampDate = $newDate->getTimestamp();
                // Register others lessons
                    if (ServiceHolidays::holidays($timestampDate, $zone)) {
                        // set_time_limit(20);
                        $newDate->modify('+7 day'); 
                        if(ServiceHolidays::holidays($timestampDate, $zone)){
                            $newDate->modify('+7 day'); 
                        }
                    }
                    
                    if ($newDate < $closeAt) {
                        $lesson = new Lesson();
                        $lesson->setStartAt($newDate);
                        $lesson->setRegistration($data);
                        $em->persist($lesson);
                        $em->flush();
                    }
                }
        };

        // TODO: if Lessons are recorded add a succes flash message
        return $data;
    }

   /**
    * Retrieve teachers list
    * @Route("/api/teachers", name="teachers")
    */
    public function getTeacherstList()
    {
        $em = $this->getDoctrine()->getRepository(User::class);
        $teachers = $em->findByRole("ROLE_TEACHER");
        sizeof($teachers);
        $data = [];

        for ($i = 0; $i < sizeof($teachers); $i++) {
            $data[] = [ 'id' => 'api/users/'.$teachers[$i]->getId(), 'name' => $teachers[$i]->getFirstname() . ' '.   $teachers[$i]->getLastname()];
        }

        return  new JsonResponse(
            $data
              
        );
    }

     /**
    * Retrieve student list
    
    * @Route("/api/students", name="students")
    */
    public function getStudentsList()
    {
        $em = $this->getDoctrine()->getRepository(User::class);
        $teachers = $em->findByRole("ROLE_STUDENT");
        sizeof($teachers);
        $data = [];
        for ($i = 0; $i < sizeof($teachers); $i++) {
            $data[] = [ 'id' => 'api/users/'.$teachers[$i]->getId(), 'name' => $teachers[$i]->getFirstname() . ' '.   $teachers[$i]->getLastname()];
        }
        return  new JsonResponse(
            $data   
        );
    }

    

}