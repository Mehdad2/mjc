<?php
namespace App\Controller;

use App\Service\UserService;
use App\Repository\LessonRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LessonController extends AbstractController
{
    /**
    * Pour l'administrateur, montre toutes les lessons d'une journée
    * @Route("/lessons/{date}", name="date")
    */
    public function dateAction(Request $request)
    {
        $dateRequest = $request->get('date');
        $date = new \DateTime($dateRequest);
        $em = $this->getDoctrine()->getManager();
        $lessons = $em->getRepository('App:Lesson')->getLessonsFromDate($date);
        // dd($lessons);
        // return new JsonResponse($lessons);

        // TODO: Faire un json avec dedans :

        return $this->render('admin/date.json.twig', [
            'lessons' => $lessons,
        ],
        new JsonResponse()
          );
    }
}