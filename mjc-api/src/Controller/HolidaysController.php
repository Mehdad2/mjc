<?php
namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// use App\Service\HolidayService;

class HolidaysController extends AbstractController
{

     /**
     * @Route(
     *     name="holiday",
     *     path="/holiday/",
     *     methods={"GET"},
     * )
     */
    public function isHoliday()
    {
        // https://calendarific.com/api/v2/holidays?api_key=7d704716ba940fddcf396893e0e7c6ec3f13b131&country=fr&year=2019
        
        $curl = curl_init('https://jours-feries-france.antoine-augusti.fr/api/2019/?date=2019-01-05');
        $country = "&country=fr";
        $year = "&year=2019";
        // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($curl);
        if ($data === false) {
            var_dump(curl_error($curl));
        } else {
            curl_close($curl);
            return New JsonResponse($data);
            // $data = json_decode($data, true);
            // echo '<pre>';
            // var_dump($data);
            // echo '</pre>';
        }

        curl_close($curl);
        
        // $statusCode = $response->getStatusCode();
        // // $statusCode = 200
        // $contentType = $response->getHeaders()['content-type'][0];
        // // $contentType = 'application/json'
        // $content = $response->getContent();
        // // $content = '{"id":521583, "name":"symfony-docs", ...}'
        // $content = $response->toArray();
        // // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]

        // if ($statusCode = 200) {

        //     return New JsonResponse($content, $contentType);
        // }
    }

}
 /** Endpoint = https://calendarific.com/api/v2
    * API_KEY=7d704716ba940fddcf396893e0e7c6ec3f13b131
    * curl -G https://calendarific.com/api/v2/holidays?api_key=7d704716ba940fddcf396893e0e7c6ec3f13b131
    * https://calendarific.com/client-libraries#examples
    * Problème avec cette API la gratuité veut que l'on ne fasse pas plus de 1000 requêtes par jour
    **/   