<?php
namespace App\EventListener;
use Symfony\Component\HttpFoundation\RequestStack;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTEncodedEvent;
use App\Security\SessionCrypt;
use App\Service\UserService;
use Defuse\Crypto\Key;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\MigratingSessionHandler;


class JWTCreatedListener extends SessionCrypt{
    /**
     * @var RequestStack
     */
    private $requestStack;

    private $session;
    /**
     * @var UserService
     */
    private $service;
    /**
      * @var SessionHandlerInterface $handler
      */
      public $handler;
     /**
      * @param SessionCrypt $crypt
      */
     
     private $crypt;
    
 
    public function __construct(
        RequestStack $requestStack, 
        SessionInterface $session,
        UserService $service, 
        \SessionHandlerInterface $handler
    )
    {
        $this->requestStack = $requestStack;
        $this->session = $session;
        $this->service = $service;
        $this->handler = $handler;
      
    }
    /**
 * @param AuthenticationSuccessEvent $event
 */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();
        $username = $user->getUsername();
        $token = $data['token'];
        $tokenFormat = substr($token,0,11);
        $emailFormat = substr($username,0,1);
        $sessionStorage = new MigratingSessionHandler($this->handler,$this->handler);
        $sessionData = json_decode($sessionStorage->read($tokenFormat.$emailFormat));
        $sessionXsrf = $sessionData->xsrfToken;
        $data['xsrfToken'] = $sessionXsrf;
        $event->setData($data);
    
    }


    
        /**
     * @param JWTEncodedEvent $event
     */
    public function onJwtEncoded(JWTEncodedEvent $event)
    {
        $data = [];
        $token = $event->getJWTString();
        $email = $this->service->getCurrentUser()->getUsername();
        $userId = $this->service->getCurrentUser()->getId();
        $key = \Defuse\Crypto\Key::createNewRandomKey();
        /**
         * @var $Sessioncrypt instanciate SessionCrypt which contain method for decrypt
         * @var $cryptToken encrypt payload
         * @var object $xsrftoken  add to crypt token an uniqId
         */
        $Sessioncrypt = new SessionCrypt($this->handler,$key);
        $cryptToken = \Defuse\Crypto\Crypto::encrypt(json_encode($token), $key);
        $xsrfToken = $cryptToken.uniqid();
        $sessionStorage = new MigratingSessionHandler($this->handler,$this->handler);
        $tokenFormat = substr($token,0,11);
        $emailFormat = substr($email,0,1);
        $sessionData = array("userId" => $userId, "token" => $token, "xsrfToken" => $xsrfToken);

        session_start();
        $sessionStorage->write($tokenFormat.$emailFormat,json_encode($sessionData));
    }
}