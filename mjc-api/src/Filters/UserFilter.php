<?php
namespace App\Filters;
use Doctrine\ORM\Mapping\ClassMetaData; 
use Doctrine\ORM\Query\Filter\SQLFilter; 
use Doctrine\Common\Annotations\Reader; 
 
class UserFilter extends SQLFilter 
{ 
     protected $reader; 
 
     public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias) 
     { 
        if (empty($this->reader)) {
            return '';
        }
 
        // The Doctrine filter is called for any query on any entity
        // Check if the current entity is "CountryCheck" (marked with an annotation)
        $user = $this->reader->getClassAnnotation(
            $targetEntity->getReflectionClass(),
            'App\\Annotations\\UsersByRole'
        );
         
        if (!$user) {
            return '';
        }
 
        // FieldName parameter in annotation
        $fieldName = $user->userbyrole;
         
        try {
            // Don't worry, getParameter automatically quotes parameters
            $userId = $this->getParameter('user_id');
        } catch (\InvalidArgumentException $e) {
            // No user_id has been defined
            return '';
        }
 
        if (empty($fieldName) || empty($userId)) {
            return '';
        }
 
        // Add the Where clause in the request
        $query = sprintf('(%s.%s = %s OR %s.%s IS NULL)', $targetTableAlias, $fieldName, $countryId, $targetTableAlias, $fieldName);
 
        return $query;
    }
 
    public function setAnnotationReader(Reader $reader)
    {
        $this->reader = $reader;
    }
}