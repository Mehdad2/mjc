<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;

class CreateUserCommand extends ContainerAwareCommand 
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-user';
    private $container;
    private $entityManager;
    private $passwordEncoder;
    
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passencoder)
    {
        parent::__construct();
        $this->entityManager = $em;
        $this->passwordEncoder = $passencoder;
    }


    


        protected function execute(InputInterface $input, OutputInterface $output)
        {

        $output->writeln([
            'User Creator',
            '============',
            ''
        ]);
            
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: index(EntityManagerInterface $entityManager)
        //$entityManager = $this->getContainer()->get('doctrine')->getManager();
        $user = new User();

            // retrieve the argument value using getArgument()
        $firstname = $input->getArgument('firstname');
        $lastname = $input->getArgument('lastname');
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $isActive = (bool)$input->getArgument('isActive');
        $passwordEncoded = $this->passwordEncoder->encodePassword($user, $password);
        // $birthDate = $input->getArgument('birthDate');
        // $phoneNumber = $input->getArgument('phoneNumber');
        // $streetNumber = $input->getArgument('streetNumber');
        // $streetName = $input->getArgument('streetName');
        // $zipCode = $input->getArgument('zipCode');
        $role = $input->getArgument('role');

        $output->writeln('FirstName: '.$firstname);
        $output->writeln('LastName: '.$lastname);
        $output->writeln('Email: '.$email);
        $output->writeln('Password: '.$password);
       // $output->writeln('isActive: '.$isActive);
        // $output->writeln('BirthDate: '.$birthDate);
        // $output->writeln('PhoneNumber: '.$phoneNumber);
        // $output->writeln('StreetNumber: '.$streetNumber);
        // $output->writeln('StreetName: '.$streetName);
        // $output->writeln('ZipCode: '.$zipCode);
       // $output->writeln('Role: '.$role);

        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setEmail($email);
        $user->setPassword($passwordEncoded);
        $user->setisActive($isActive);
        // $user->setBirthDate($birthDate);
        // $user->setPhoneNumber($phoneNumber);
        // $user->setStreetNumber($streetNumber);
        // $user->setStreetName($streetName);
        // $user->setZipCode($zipCode);
        $user->setRoles($role);




         // tell Doctrine you want to (eventually) save the Product (no queries yet)
         $this->entityManager->persist($user);

         // actually executes the queries (i.e. the INSERT query)
         $this->entityManager->flush();
    }


    protected function configure()
    {
        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('Creates a new user.')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command allows you to create a user...');

        $this
        // configure an argument
        ->addArgument('firstname', InputArgument::REQUIRED, 'The user firstname. string')
        ->addArgument('lastname', InputArgument::REQUIRED, 'The user lastname. string')
        ->addArgument('email', InputArgument::REQUIRED, 'The email of the user. string')
        ->addArgument('password', InputArgument::REQUIRED, 'The password of the user.')
        ->addArgument('isActive', InputArgument::REQUIRED, 'Activate the user boolean 1 or 0')
        // ->addArgument('birthDate', InputArgument::OPTIONAL, 'The birthdate of the user.')
        // ->addArgument('phoneNumber', InputArgument::OPTIONAL, 'The Phone of the user.')
        // ->addArgument('streetNumber', InputArgument::OPTIONAL, 'The street number of the user.')
        // ->addArgument('streetName', InputArgument::OPTIONAL, 'The street name of the user.')
        // ->addArgument('zipCode', InputArgument::OPTIONAL, 'The zip code of the user.')
        ->addArgument('role',InputArgument::IS_ARRAY, 'The role of the user. ROLE_ADMIN, ROLE_STUDENT, ROLE_TEACHER, ROLE_GUEST');


    }
    
}