<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\User;
use App\Entity\Email;

class SendEmailCommand extends ContainerAwareCommand 
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:send-email';
    private $mailer;
    private $entityManager;
    private $container;
    public function __construct(EntityManagerInterface $em, \Swift_Mailer $mailer, ContainerInterface $container)
    {
        parent::__construct();
        $this->entityManager = $em;
        $this->mailer = $mailer;
        $this->container = $container;
    }


        protected function execute(InputInterface $input, OutputInterface $output)
        {
            $user = $this->entityManager->getRepository(User::class)->findBy(['isActive' => false]);
            foreach ($user as $value) {
                $email = new Email();
                var_dump($value->getEmail());
                $message = (new \Swift_Message('Test Active your account'))
                ->setFrom('plannersjcm@gmail.com')
                ->setTo($value->getEmail())
                ->setBody(
                    $this->container->get('twig')->render(
                        // templates/emails/registration.html.twig
                        'emails/activateAccount.html.twig',
                        ['firstname' => $value->getFirstName(), 'lastname' => $value->getLastname()]
                    ),
                    'text/html'
                )
        
                // For text version
                /*->addPart(
                    $this->renderView(
                        'emails/registration.txt.twig',
                        ['name' => $name]
                    ),
                    'text/plain'
                )*/
            ;
                var_dump($value->getFirstname());
                $email->setModel('emails/activateAccount.html.twig');
                $email->setUser($value);
                $this->entityManager->persist($email);

          //mettre le service dans un cron

            //$idEmailActivateAccount = $this->entityManager->getRepository(Email::class)->findBy(['model' => "emails/activateAccount.html.twig"]);
           // $value->addEmailId(new Email());

        // actually executes the queries (i.e. the INSERT query)

         $this->mailer->send($message);
          
          /*

         // tell Doctrine you want to (eventually) save the Product (no queries yet)
         $this->entityManager->persist($user);

         // actually executes the queries (i.e. the INSERT query)
         $this->entityManager->flush();
         */
       
    }
    $this->entityManager->flush();
   
}


   
    
}