# planner

## Cahier des charges

### Préambule

Dans le cadre d'une demande émanant de la MJC, en vue de gérer au mieux l'emploi du temps des professeurs et des étudiants. La plateforme doit permettre l'interraction entre ces deux acteurs, de sorte que professeurs et èleves puissent échanger et se tenir informés sur les disponibilités aux sessions de cours (absences/présences) ou encore sur le contenu des cours.
Cet outil doit permettre également à ces deux acteurs de transmettre leurs remarques aux responsables de la MJC à savoir, appréciations des cours pour les élèves. Les observations ou remarques d'un professeur au sujet d'un cours seront accessibles aux parents et élèves.
  
### Acteurs et Rôles : Les usages de l'outil

- La secrétaire de la MJC ou les responsables en vue de mieux gérer le flux des élèves et des enseignants de leur organisme.
- Le professeur en vue de rester informer de son emploi du temps et d'avoir des interractions continues avec ses élèves jusqu'aux prochaines sessions de cours.
- L'élève afin de rester informé des prochaines sessions de cours particuliers ou collectif, et notamment en vue d'interragir avec ses professeurs et camarades.

### Besoins
  
- Les reponsables de la MJC doivent pouvoir saisir les emplois du temps des acteurs, ces emplois du temps concernent un élève, un professeur mais également un groupe d'élève.
- Les Professeurs et les élèves consulteront leur planning en temps réel.
- Les Professeurs et les élèves pourront indiquer leurs indisponibilités et **proposer des dates de rattrapages.**
- Les Professeurs et élèves peuvent faire des remontées aux responsables de la MJC au sujet d'un cours, d'un professeur / élève,...
- La plateforme doit être multi-plateformes (web / desktop et mobile)
- Les responsables de la MJC doivent pouvoir indiquer des évennements culturels ou annonces visibles par les acteurs, soit de façon ciblée (acteurs en particulier) ou communes à ces derniers.
- La gestion des absences doit faire l'objet de notifications d'alerte à plusieurs niveaux.
- Les Professeurs pourront laisser des appréciations / remarques à l'attention de leurs élèves dans un espace dédié.
- Faciliter la recherche des sessions de cours avec un professeur ou un élève.
  
### Enjeux

- La sécurité et la gestion des droits d'accès au contenu des comptes utilisateurs. La création d'un compte utilisateur de la part d'un responsable ne doit pas lui permettre d'avoir accès aux échanges professeurs / élèves.
- Une recherche organisée et rapide des sessions de cours.
- La prise en compte qu'un élève puisse suivre un cours en session collective (rattaché à un groupe).
- Les imprévus doivent faire l'objet d'alertes au niveau interne (messagerie interne) et externe (envoie sur de mails sur la messagerie externe des acteurs)
- Faciliter l'insertion des emplois du temps aux personnes en charge de cette mission.
  
 ### Macrofonctionnalité






