# Analyse Fonctionnelle

## I. Système interne de l'application

- Création des comptes administrateurs (en mode manuel) pour la gestion administratives des acteurs (profeseurs / élèves) de la MJC.
- API par services en vue de récupérer les données mais aussi de déclencher les actions adéquates.
  - Service d'Authentification
  - Service de Notification
  - Service de gestion des leçons( prise en compte des vacances scolaire)
  - Service Helper Bot
  - Service de messagerie
  - Service d'accès aux observations pédagogiques
  - Service de validation des comptes utilisateurs
  - Service de recherche ( cours, professeurs, élèves,...)
  - Service de pré-inscription
    - Sous service upload
    - Sous service importation
      -Validation du format CSV
      -Validation du typage de données insérées
    - Sous service édition
    - Sous service multi-import
    - Sous service Export
  - Service de stockage des données

## II. Connexion

- Interface d'authentification (identifiant, mot de passe) et gestion en cas d'oubli de mot de passe
  
### Prérequis à la connexion pour professeurs et élèves

- Réception du mail d'accès avec lien pour la validation du mot de passe à la première connexion
- Notification par mail des changements relatifs au compte  et liens d'accès à l'application (multi-plateformes)

## III. Back office d'un administrateur MJC

### 1. Menu administrateur

- Gestion des pré-inscriptions*(les fonctionnalités ornées d'une étoile seront déclenchés à l'issue de la validation définitive des préinscriptions)
- Création des spécialités de cours par l'administrateur
- *Création d'un compte élève, professeur avec pour chacun leur rôle respectif (en mode manuel et automatique)
- *Souscription des élèves et des professeurs à une session de spécialités (cours particuliers et collectifs), en mode manuel et automatique et de ce fait création des leçons de cours associées aux différentes inscriptions (professeurs / élèves)
- Visualisation et exportation d'un tableau informatif relatif aux élèves / professeurs / spécialités ...
- Création d'événements.
- Paramétrage et personnalisation du profil (mot de passe,...)
- Modification du compte

### 2. Dashboard

- DashBoard du planning des cours de la journée ou de la semaine
  - Recherche / filtre par date, par intitulés (spécialités), par identifiant d'élèves et ou professeurs
  - Notification d'annulation de cours visible sur le dashboard de la journée (La couleur de la notification dépendra de l'acteur ayant annulé)
  - Accès au menu administrateur
  - Déconnexion du compte administrateur (redirection sur la page login)
- Pop up :à l'arrivée de la date de fermeture de la MJC (exemple :"veuillez renseigner les dates d'ouverture et de fermeture pour l'année à venir")

## IV. Front Office des Elèves  et professeurs

### 1. Menu

- Paramétrages  et personnalisation du profil (mot de passe, notifications,...)
- Visualisation de la liste de tous les cours
- Accès aux observations (édition /lecture pour professeur, lecture pour élèves)
- Demande de rattrapage de cours
- Professeur : possibilité de modifier de manière permanente ses disponibilités sur un ou des cours dans l'années
  
### 2. Dashboard

- Dashboard du planning des cours de la journée ou de la semaine de l'acteur concerné et des évènements MJC
- Possibilité d'annulation des cours (Au préalable, confirmation d'annulation )
- Possibilité de décaler une leçon 
- Notification d'annulation des cours visibles sur le dashboard.
- Recherche / filtre des cours par date, professeurs / élèves ou spécialités.
- Compteur de notifications

## V. Helper

### 1. Administrateur

- L'administrateur est notifié des demandes /remarques émanant des professeurs et/ ou élèves en direct live (chat)
- L'administrateur est notifié des demandes /remarques émanant des professeurs et/ ou élèves sur sa messagerie interne

### 2. Elèves / Professeurs

- L'utilisateur connecté peut poser une question à l'administration de la MJC

### 3. Bot

- Des réponses génériques pourront être envoyées automatiquement pour toutes les questions communes
- Si le Bot ne peut pas répondre, redirection vers l'administrateur (messagerie interne ou externe).
